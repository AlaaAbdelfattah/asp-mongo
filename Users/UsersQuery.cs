using GraphQL.Types;
using GraphQLNetExample.Services;
namespace GraphQLNetExample.Users;

public class UsersQuery : ObjectGraphType
{
    // private readonly UsersService _usersService;
    public UsersQuery()
    {
        //  async Task<List<User>> Get() =>
        //      await _usersService.GetAsync();
        
        //return a list of dumb data, should read from mongoDB here
        Field<ListGraphType<UserType>>("Users", resolve: context => new List<User> {
        new User { Id = Guid.NewGuid(), Name = "Alaa" , Age="23"},
        new User { Id = Guid.NewGuid(), Name = "Test" , Age="19"},
              new User { Id = Guid.NewGuid(), Name = "asw" , Age="50"}
        });
    }
}