using GraphQL;
using GraphQL.Types;

namespace GraphQLNetExample.Users
{
    public class UsersMutation : ObjectGraphType
    {
        public UsersMutation()
        {
            Field<UserType>(
                "createUser",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "name" }
                ),
                resolve: context =>
                {
                    var name = context.GetArgument<string>("name");
                    var user = new User
                    {
                        Id = Guid.NewGuid(),
                        Name = name,

                    };
                    // Should add the user to mongoDB here
                    return user;
                }
            );
        }
    }
}
