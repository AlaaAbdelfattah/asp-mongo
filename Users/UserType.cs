using GraphQL.Types;

namespace GraphQLNetExample.Users;

public class UserType : ObjectGraphType<User>
{
    public UserType()
    {
        Name = "User";
        Description = "User Type";
        Field(d => d.Id, nullable: false).Description("User Id");
        Field(d => d.Name, nullable: true).Description("User Name");
        Field(d => d.Age, nullable: true).Description("User Age");

    }
}
