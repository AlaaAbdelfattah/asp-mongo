using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GraphQLNetExample.Users;

public class User
{
    // [BsonRepresentation(BsonType.ObjectId)]
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Age { get; set; }

}
