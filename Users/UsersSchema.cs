using GraphQL.Types;

namespace GraphQLNetExample.Users;

public class UsersSchema : Schema
{
  public UsersSchema(IServiceProvider serviceProvider) : base(serviceProvider)
  {
    Query = serviceProvider.GetRequiredService<UsersQuery>();
     Mutation = serviceProvider.GetRequiredService<UsersMutation>();
  }
}
