using GraphQLNetExample.Users;
using GraphQLNetExample.Services;
using Microsoft.AspNetCore.Mvc;

namespace GraphQLNetExample.Controllers;

public class UsersController : ControllerBase
{
    private readonly UsersService _usersService;

    public UsersController(UsersService usersService) =>
        _usersService = usersService;

    [HttpPost]
    public async Task<List<User>> Get() =>
        await _usersService.GetAsync();

    [HttpPost]
    public async Task<IActionResult> Post(User newUser)
    {
        await _usersService.CreateAsync(newUser);

        return CreatedAtAction(nameof(Get), new { id = newUser.Id }, newUser);
    }

}